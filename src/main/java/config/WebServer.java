package config;

public class WebServer {
  
  private static final String PORT = "9999";
  private static final String IP = "localhost";
  public static final String BASE_URL = "http://" + IP + ":" + PORT;

}
