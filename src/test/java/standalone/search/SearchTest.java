package standalone.search;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;

import config.WebServer;

/**
 * Test uses the standalone server
 */
public class SearchTest {

  @Test
  public void should_find_free_stuff() {
    given().
    //stel dat mijn search_term "freestuff" is
    param("search_term", "").
    when().
    get(WebServer.BASE_URL + "/search").
    then().
    assertThat().
    // wat moet in de body staan? kijk op BASE_URL/search?search_term=freestuff
    body(equalTo(""))
    .and()
    //content-type is text/plain
    .header("Content-Type", equalTo(""))
    //geen cache control
    .header("Cache-control", equalTo(""));
  }

}
