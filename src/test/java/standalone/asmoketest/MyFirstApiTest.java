package standalone.asmoketest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;

import config.WebServer;

/**
 * Test uses the standalone server
 */

public class MyFirstApiTest {

  @Test
  public void smokeTest() {
    given().
    when().
    //doe een request naar de webserver op deze URL
      get(WebServer.BASE_URL + "/smoketest").
    then().
    //laat in de console zien wat de body van de response is
    log().
      body().
    assertThat().
    //check dat de body exact deze twee woorden bevat
      body(equalTo("Some content"));
  }

}
