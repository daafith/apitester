package standalone.login;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;

import config.WebServer;

/**
 * Test uses the standalone server
 */
public class LoginTest {

  @Test
  public void should_login() {
    given().   
    //stel dat ik een authenticatie wil testen
      auth().
      preemptive().
      //stel dat het BASIC authentication is username "admin" en password "qwerty123"
      basic("", "").
    when().
      post(WebServer.BASE_URL + "/login").
    then().
      assertThat().
      //dan moet ik "Logged in as admin" in de body terug zien
      body(equalTo(""));
  }

}
