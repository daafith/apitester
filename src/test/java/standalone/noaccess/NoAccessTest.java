package standalone.noaccess;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import org.junit.Test;

import config.WebServer;

/**
 * Test uses the standalone server
 */
public class NoAccessTest {

  @Test
  public void should_deny_access() {
    given().
    when().
    //doe een request naar de webserver op deze URL
      get(WebServer.BASE_URL + "/noaccess").
    then().
    assertThat().
      //moet 403 zijn
      statusCode(0)
      .and()
      // Moet "You have no access! Are you a hacker?" bevatten
      .statusLine(containsString(""));
  }
}
